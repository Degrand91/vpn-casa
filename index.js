var spawn = require("child_process").spawn
var express = require("express")
var app = express()

app.use(express.static(__dirname))

app.get("/connect", function (req, res) {
	console.log(req.query.location)
	if (req.query.location === "---") return res.send(500)
	var command = spawn(__dirname + "/run.sh", [req.query.location || ""])
	var output = []

	command.stdout.on("data", function (chunk) {
		output.push(chunk)
	})

	command.on("close", function (code) {
		if (code === 0) res.send(Buffer.concat(output))
		else res.send(500) // when the script fails, generate a Server Error HTTP response
	})
})

app.listen(3000)
